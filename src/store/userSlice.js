
import { createSlice } from '@reduxjs/toolkit';

let user = createSlice({
    name : 'user',
    initialState : {name : 'kim', age: 20},
    reducers : {
      changeName(state){//state : 기존 state를 뜻함
        //return {name : 'park', age: '20'}
        state.name = 'park' // array/object의 경우 직접수정해도 stat변경됨 : immer.js 의 도움으로
      },
      increase(state, action){ 
        state.age += action.payload //payload: 화물 action으로 파라미터작명을 많이함(state 변경함수를 action이라고 하므로)
      }
    }

})

export let {changeName, increase} = user.actions 

export default user