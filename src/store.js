import { configureStore ,createSlice } from '@reduxjs/toolkit';
import user from './store/userSlice.js'

/* 
let user = createSlice({
    name : 'user',
    initialState : {name : 'kim', age: 20},
    reducers : {
      changeName(state){//state : 기존 state를 뜻함
        //return {name : 'park', age: '20'}
        state.name = 'park' // array/object의 경우 직접수정해도 stat변경됨 : immer.js 의 도움으로
      },
      increase(state, action){ 
        state.age += action.payload //payload: 화물 action으로 파라미터작명을 많이함(state 변경함수를 action이라고 하므로)
      }
    }

}) 


*/

let stock = createSlice({
    name : 'stock',
    initialState : ['10', '11', '12']
})

let cart = createSlice({
  name : 'cart',
  initialState : [
    {id : 0, name : 'White and Black', count : 2},
    {id : 2, name : 'Grey Yordan', count : 1}
   
  ],
  reducers : {
    addCount(state,action){ 
      //state[i].count += 1 //payload: 화물 action으로 파라미터작명을 많이함(state 변경함수를 action이라고 하므로)
     // console.log(state[action.payload].count += 1);
     // console.log(state[action.payload].count++);
      //console.log(state[action.payload].id)
    /*
     let 번호 = state.findIndex((a)=>{
      return a.id  == action.payload
     })
    state[번호].count++;
    */
      //console.log(state[번호].id)
      let number = state.findIndex((a)=>{
        return a.id  == action.payload
        })
      state[number].count++;     
      //payload 와 같은 id 가진 상품을 찾아서 +1

    },
    
    addItem(state,action){
      let number = state.findIndex((a)=>{
        return a.id  === action.payload.id
       })
       if(number > -1 ) {

        state[number].count++

      } else {

        state.push(action.payload)

      }

    }
  }
})

export let {addCount, addItem} = cart.actions 



export default configureStore({
  reducer: { 
    user : user.reducer,
    stock : stock.reducer,
    cart : cart.reducer
  }
}) 

