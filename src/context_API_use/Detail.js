
import React, {useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {Nav} from 'react-bootstrap';
import {Context1} from './../App.js';

function Detail(props){

let {stock} = useContext(Context1);


  let {id} = useParams();
  let findItem = props.shoes.find((x)=>{
    return x.id == id;
  })

  let [timer, setTimer] = useState(true);
  let [count, setCount] = useState(0);
  let [num, setNum] = useState(false);
  let [tab, setTab] = useState(0);

  useEffect(()=>{

  if(isNaN(num) == true){
    alert('숫자로 입력해주세요')
  }
  },[num])//num 이 변경시에만 실행하려면

  useEffect(()=>{
   let a = setTimeout(()=>{setTimer(false)},2000)
   //console.log(2);
   return()=>{
    //console.log(1);
    clearTimeout(a);
    }
  }, [])



  let [fade2, setFade2] = useState('');
  useEffect(()=>{
    setTimeout(()=>{setFade2('end')}, 1000)

    return ()=>{
      setFade2('')
    }
  },[])


    return(
      <div className={"container start " + fade2 }>

        {
          timer == true ? <Timer></Timer> : null
        }

        <div className="row">
          <div className="col-md-6">
            <img src={'https://codingapple1.github.io/shop/shoes'+ (findItem.id + 1) +'.jpg' } width="100%" />
            
          </div>
          {stock}
          <div className="col-md-6 mt-4">
            <h4 className="pt-5">{findItem.title}</h4>
            <p>{findItem.content}</p>
            <p>{findItem.price}원</p>
            <button className="btn btn-danger">주문하기</button> 
          </div>
        </div>

        <Nav variant="tabs"  defaultActiveKey="link0">
          <Nav.Item>
            <Nav.Link eventKey="link0" onClick={()=>{
                setTab(0)
              }
            }>버튼0</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="link1" onClick={()=>{
                setTab(1)
              } }>버튼1</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="link2" onClick={()=>{
                setTab(2)
              } }>버튼2 </Nav.Link>
          </Nav.Item>
        </Nav>
  {/*       {
          tab == 0 ?  <div>내용0</div> : null
        }
        {
          tab == 1 ?  <div>내용1</div> : null
        }
        {
          tab == 2 ?  <div>내용2</div> : null
        }
        */}


        {/* <input onChange={(e)=>{setNum(e.target.value)}}></input>
        <button className="btnNum" onClick={()=>{setCount(count+1)}}>수량버튼</button>
        {count} */}

        <TabContent tab={tab}/>

      </div> 

    )
  }

  function TabContent({tab}){
    let [fade, setFade] = useState('');
    let {stock} = useContext(Context1);
    useEffect(()=>{
      setTimeout(()=>{setFade('end')}, 100)

      return ()=>{
        setFade('')
      }
    },[tab])
  
    return ( <div className={'start ' + fade}>
      {[<div>{stock}</div>, <div>내용1</div>, <div>내용2</div>][tab]}
    </div>)
    
   }
 

/*
  function TabContent(props){
    if(props.tab == 0){
      return <div>내용0</div>
    }
    if(props.tab == 1){
      return <div>내용1</div>
  
    }
    if(props.tab == 2){
      return <div>내용2</div>
    }
  }
*/
//props를 쓰지 않고 상속시에는 아래처럼 가능
/*
   function TabContent({tab}){
    if(tab == 0)
    {
      return <div>내용0</div>
    }
    if(tab == 1){
      return <div>내용2</div>
    }
    if(tab == 2){
      return <div>내용3</div>
    }

  }
*/


  function Timer(){
    return(
      <div className="alert alert-warning">
        2초이내 구매시 할인
      </div>
    )
    
  }
  export default Detail;