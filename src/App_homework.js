
import {useEffect, useState} from "react";
import {Button,Navbar,Container,Nav} from 'react-bootstrap';
import './App.css';
import shoes1 from './img/shoes1.jpg';
import shoes2 from './img/shoes2.jpg';
import shoes3 from './img/shoes3.jpg';
/* import 작명 from './data.js'; */
import data from './data.js';
import {Routes, Route, useNavigate, Outlet} from 'react-router-dom';
import Detail from "./routes/Detail.js";
import axios from "axios";

function App() {

  let [shoes] = useState(data);
  let [i] = useState(0);

  let navigate = useNavigate();

  let [bts, setBts] = useState([]);

  useEffect(()=>{
    axios.get('https://codingapple1.github.io/shop/data2.json')
    .then((결과)=>{
      //console.log(결과.data);
     
      console.log(setBts(결과.data))
    })
    .catch(()=>{
      console.log('전송실패')
    })
    
  },[]);


  return (
    <div className="App">


      <Navbar bg="dark" variant="dark" className='navMenu'>
        <Container>
          <Navbar.Brand href="#home">exShop</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link onClick={()=>{navigate('/')}}>Home</Nav.Link>
            <Nav.Link onClick={()=>{navigate('/detail')}}>Detail</Nav.Link>
            <Nav.Link onClick={()=>{navigate('/about')}}>about</Nav.Link>
          </Nav>
        </Container>
      </Navbar>

      
      <Routes>
        <Route path="/" element={
          <>
            <div className='main-bg'></div>
            <div className='container'>
              <div className='row'>
                {
                  shoes.map(function(a,i){
                    return(
                      <Card shoes = {shoes[i]} i={i} key={i}></Card>
                    )
                  })
                }
              </div>
              <button onClick={()=>{
                
              }}>더보기 버튼</button> 
               
               <div className='container'>
                <MoreItem bts={bts}></MoreItem> 
              </div>


            </div> 
          </>
        }/>
        <Route path="/detail/:id" element={<Detail shoes={shoes}></Detail>} />


        <Route path="/about" element={<About/>}>
          <Route path="member" element={<div>멤버</div>} />
          <Route path="location" element={<div>위치정보</div>} />
        </Route>
        
        <Route path="/event" element={<Event/>}>
          <Route path="one" element={<div>첫 주문시 양배추즙 서비스</div>} />
          <Route path="two" element={<div>생일기념 쿠폰받기</div>} />
        </Route>

        <Route path="*" element={<div>없는 페이지입니다.</div>} />
      </Routes>

    </div>
  );
}


function MoreItem({bts}){
  return(
    <div class="row">
      {
        bts.map((bt,i) => {
            return (
              <div className='col-md-4' key={i}> 
                <img src={'https://codingapple1.github.io/shop/shoes'+ (bt.i+1) + '.jpg'} width="80%"/>
                  <h4>{bt.title} </h4>
                  <p>{bt.price}</p>
              </div>
                  )
              })
            }
    </div>
  )
}

function Event(){
  return(
    <div>
      <h4>오늘의 이벤트</h4>
      <Outlet></Outlet>
    </div>
  )
}


function About(){
  return(
    <div>
      <h4>회사정보</h4>
      <Outlet></Outlet>
    </div>
  )
}



function Card(props){
  return(
    <div className='col-md-4'>
      <img src={'https://codingapple1.github.io/shop/shoes'+ (props.i+1) + '.jpg'} width="80%"/>
        <h4>{props.shoes.title} </h4>
        <p>{props.shoes.price}</p>
    </div>
  )
}


export default App;
