
import {Button,Navbar,Container,Nav} from 'react-bootstrap';
import './App.css';
import bg from './img/bg.png'

function App() {
  return (
    <div className="App">
      <Navbar bg="dark" variant="dark" className='navMenu'>
        <Container>
          <Navbar.Brand href="#home">exShop</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="#home">home</Nav.Link>
            <Nav.Link href="#cart">cart</Nav.Link>
            <Nav.Link href="#mypage">mypage</Nav.Link>
          </Nav>
        </Container>
      </Navbar>

      <div className='main-bg' style={{backgroundImage : 'url('+bg+')'}}>

      </div>

    </div>
  );
}

export default App;
