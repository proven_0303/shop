
import {useState} from "react";
import {Button,Navbar,Container,Nav, Row, Col} from 'react-bootstrap';
import './App.css';
import shoes1 from './img/shoes1.jpg';
import shoes2 from './img/shoes2.jpg';
import shoes3 from './img/shoes3.jpg';
/* import 작명 from './data.js'; */
import data from './data.js';


function App() {

  let [shoes] = useState(data)


  return (
    <div className="App">


      <Navbar bg="dark" variant="dark" className='navMenu'>
        <Container>
          <Navbar.Brand href="#home">exShop</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="#home">home</Nav.Link>
            <Nav.Link href="#cart">cart</Nav.Link>
            <Nav.Link href="#mypage">mypage</Nav.Link>
          </Nav>
        </Container>
      </Navbar>

      <div className='main-bg'></div>

      {/* <div className='container'><!-- 이건 리액트 부트스트랩아니고 그냥 부트스트랩에서 직접 가져온것 : 강의내용-->
        <div className='row'>
          <div className='col-md-4'>안녕</div>
          <div className='col-md-4'>안녕</div>
          <div className='col-md-4'>안녕</div>
        </div>
      </div> */}


      <Container>
        <Row>
          <Col>
            <img src={process.env.PUBLIC_URL+'/logo192.png'} style={{width : '80%'}} />
            <h4>{shoes[0].title}</h4>
            <p>{shoes[0].price}</p>
          </Col>
          <Col>
            <img src={shoes2} style={{width : '80%'}} />
            <h4>{shoes[1].title}</h4>
            <p>{shoes[1].price}</p>
          </Col>
          <Col>
            <img src={shoes3} style={{width : '80%'}}/>
            <h4>{shoes[2].title}</h4>
            <p>{shoes[2].price}</p>
          </Col>
        </Row>
      </Container>


    </div>
  );
}

export default App;
