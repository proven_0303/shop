
import {useState} from "react";
import {Button,Navbar,Container,Nav, Row, Col} from 'react-bootstrap';
import './App.css';
import shoes1 from './img/shoes1.jpg';
import shoes2 from './img/shoes2.jpg';
import shoes3 from './img/shoes3.jpg';
/* import 작명 from './data.js'; */
import data from './data.js';
import {Routes, Route, Link} from 'react-router-dom'



function App() {

  let [shoes] = useState(data)
  let [i] = useState(0)


  return (
    <div className="App">



      <Navbar bg="dark" variant="dark" className='navMenu'>
        <Container>
          <Navbar.Brand href="#home">exShop</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="#home">home</Nav.Link>
            <Nav.Link href="#cart">cart</Nav.Link>
            <Nav.Link href="#mypage">mypage</Nav.Link>
          </Nav>
        </Container>
      </Navbar>

      
      <Routes>
        <Route path="/" element={<div>메인페이지입니다.</div>}/>
        <Route  path="/detail" element={<div>상세페이지 입니다.</div>}/>
        <Route path="/about" element={<div>어바웃페이지 입니다.</div>} />
      </Routes>

      <div className='main-bg'></div>

      {/* <div className='container'>
        <div className='row'>
          <div className='col-md-4'>안녕</div>
          <div className='col-md-4'>안녕</div>
          <div className='col-md-4'>안녕</div>
        </div>
      </div> */}


 {/*      <Container>
        <Row>
          <Col>
            <img src={shoes1} style={{width : '80%'}} />
            <h4>{shoes[0].title}</h4>
            <p>{shoes[0].price}</p>
          </Col>
          <Col>
            <img src={shoes2} style={{width : '80%'}} />
            <h4>{shoes[1].title}</h4>
            <p>{shoes[1].price}</p>
          </Col>
          <Col>
            <img src={shoes3} style={{width : '80%'}}/>
            <h4>{shoes[2].title}</h4>
            <p>{shoes[2].price}</p>
          </Col>
        </Row>
      </Container> */}



<div className='container'>
  <div className='row'>
    {/* {
      card.map(function(a,i){
        return(
        
            <div className='col-md-4'  key={i}>
              <img src="" style={{width : '80%'}} />
              <h4>{shoes[i].title} </h4>
              <p>{shoes[i].price}</p>
            
            </div>

        )
      })

      
    } */
    }
    {/* <Card shoes = {shoes[0]} i={1}></Card>
    <Card shoes = {shoes[1]} i={2}></Card>
    <Card shoes = {shoes[2]} i={3}></Card> */}
    {
      shoes.map(function(a,i){
        return(
          <Card shoes = {shoes[i]} i={i}></Card>
        )
      })
    }

  </div> 
</div> 



    </div>
  );
}



function Card(props){

  return(

    <div className='col-md-4'>
      <img src={'https://codingapple1.github.io/shop/shoes'+ (props.i+1) + '.jpg'} width="80%"/>
        <h4>{props.shoes.title} </h4>
        <p>{props.shoes.price}</p>
    </div>
  )
}


export default App;
