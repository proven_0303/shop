
import React, {useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {Nav} from 'react-bootstrap';
import {Context1} from './../App.js';
import {addItem} from './../store.js';
import { useDispatch, useSelector } from 'react-redux';

  function Detail(props){

  //let {stock} = useContext(Context1);
    
    let {id} = useParams();
    let findItem = props.shoes.find((x)=>{
      return x.id == id;
    })

    let [timer, setTimer] = useState(true);
    //let [count, setCount] = useState(0);
    let [num, setNum] = useState(false);
    let [tab, setTab] = useState(0);

    let state = useSelector((state)=>{ return state })
    let dispatch = useDispatch()
    //넘겨준 데이터의 id를 기준으로 페이지가 생성되어야 하기에 요청데이터는 id이고 이걸 usePrams로 받은 것

/*
    useEffect(()=>{
      //누가 detail 페이지 접속하면
      //그 페이지에 보이는 상품id 가져와서
      // localStorage에 watched 항목에 추가
      //console.log(findItem)

      //localStorage.setItem('watched',[findItem.id])
      let 꺼낸거 = localStorage.getItem('watched');
      꺼낸거 = JSON.parse(꺼낸거)
      꺼낸거.push(findItem.id);//<- 이미 있으면 push() 하지마라

      //set으로 바꿨다가 다시 array로 만들기
      꺼낸거 = new Set(꺼낸거)
      꺼낸거 = Array.from(꺼낸거)

      localStorage.setItem('watched', JSON.stringify(꺼낸거))

    },[])

*/
  //현재 상세페이지의 id값을 저장해둠
  //console.log(findItem.id)
  //let itemId = findItem.id
  //localStorage.setItem('상품이름', itemId);




    useEffect(()=>{

    if(isNaN(num) == true){
      alert('숫자로 입력해주세요')
    }
    },[num])//num 이 변경시에만 실행하려면

    useEffect(()=>{
    let a = setTimeout(()=>{setTimer(false)},2000)
    //console.log(2);
    return()=>{
      //console.log(1);
      clearTimeout(a);
      }
    }, [])

    let [fade, setFade] = useState('');
    //let {stock} = useContext(Context1);
    useEffect(()=>{
      setTimeout(()=>{setFade('end')}, 100)

      return ()=>{
        setFade('')
      }
    },[tab])

    let [fade2, setFade2] = useState('');
    useEffect(()=>{
      setTimeout(()=>{setFade2('end')}, 100)

      return ()=>{
        setFade2('')
      }
    },[])


      return(
        <div className={"container start " + fade2 }>

          {
            timer == true ? <Timer></Timer> : null
          }

          <div className="row">
            <div className="col-md-6">
              <img src={'https://codingapple1.github.io/shop/shoes'+ (findItem.id + 1) +'.jpg' } width="100%" />
              
            </div>
      
            <div className="col-md-6 mt-4">
              <h4 className="pt-5">{findItem.title}</h4>
              <p>{findItem.content}</p>
              <p>{findItem.price}원</p>
              <button className="btn btn-danger" onClick={()=>{
                //dispatch(addItem({id :3, name : 'kimmi', count : 1}))
                //dispatch(addItem())
                dispatch(addItem({id :3, name : 'kimmi', count : 1}))
              }}
              >주문하기</button> 
            </div>
          </div>

          <Nav variant="tabs"  defaultActiveKey="link0">
            <Nav.Item>
              <Nav.Link eventKey="link0" onClick={()=>{
                  setTab(0)
                }
              }>버튼0</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="link1" onClick={()=>{
                  setTab(1)
                } }>버튼1</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="link2" onClick={()=>{
                  setTab(2)
                } }>버튼2 </Nav.Link>
            </Nav.Item>
          </Nav>
    {/*       {
            tab == 0 ?  <div>내용0</div> : null
          }
          {
            tab == 1 ?  <div>내용1</div> : null
          }
          {
            tab == 2 ?  <div>내용2</div> : null
          }
          */}


          {/* <input onChange={(e)=>{setNum(e.target.value)}}></input>
          <button className="btnNum" onClick={()=>{setCount(count+1)}}>수량버튼</button>
          {count} */}

          <div className={'start ' + fade}>
            <TabContent tab={tab}/>
          </div>
          

        </div> 

      )
  }


  function TabContent({tab}){
    if(tab == 0)
    {
      return <div>내용0</div>
    }
    if(tab == 1){
      return <div>내용2</div>
    }
    if(tab == 2){
      return <div>내용3</div>
    }
  }
 

/*
  function TabContent(props){
    if(props.tab == 0){
      return <div>내용0</div>
    }
    if(props.tab == 1){
      return <div>내용1</div>
  
    }
    if(props.tab == 2){
      return <div>내용2</div>
    }
  }
*/
//props를 쓰지 않고 상속시에는 아래처럼 가능
/*
   function TabContent({tab}){
    if(tab == 0)
    {
      return <div>내용0</div>
    }
    if(tab == 1){
      return <div>내용2</div>
    }
    if(tab == 2){
      return <div>내용3</div>
    }

  }
*/
/*
 function TabContent({tab}){ 
  return ( 
    <div className={'start ' + fade}>
      {[<div>내용0</div>, <div>내용1</div>, <div>내용2</div>][tab]}
    </div>
  )
 }
*/


  function Timer(){
    return(
      <div className="alert alert-warning">
        2초이내 구매시 할인
      </div>
    )
    
  }
  export default Detail;