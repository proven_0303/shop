
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";


// 이전 방법
class Detail2 extends React.Component {
  componentDidMount(){//컴포넌트 mount시 여기 코드가 실행됨
  }
  componentDidUpdate(){//컴포넌트 update시 여기 코드가 실행됨
  }
  componentWillUnmount(){//컴포넌트 unmount시 여기 코드가 실행됨
  }
}

function Detail(props){

  let {id} = useParams();
  let findItem = props.shoes.find((x)=>{
    return x.id == id;
  })
  let [timer, setTimer] = useState(true);
  let [count, setCount] = useState(0);
  let [inputValue, inputValueChange] = useState('');
  let [num, setNum] = useState('');


  // useEffect(()=>{
  //   setTimeout(()=>{
  //     setTimer(false) //컴포넌트가 처음 장착이 되었을때(mount시), update시 이 코드가 실행됨
  //   }, 2000)
  //   console.log(1)
  // }, [count])//count 라는 state가 변할 때만 위의 코드가 실행됨

  useEffect(()=>{
    /*
    setTimeout(()=>{
      setTimer(false) //컴포넌트가 처음 장착이 되었을때(mount시), update시 이 코드가 실행됨
    }, 2000)
    */
   let a = setTimeout(()=>{
     setTimer(false)
   },2000)
   console.log(2)

   // console.log(1)
   return()=>{
    console.log(1)
    clearTimeout(a);
  
      //ex)기존 타이머는 제거해주세요, 기존 데이터요청은 제거해주세요 - 기존 코드 지우는 것을 여기 많이 작성함
     //useEffect 동작 전에 실행되는 코드임 
   }
  }, [])//이렇게 디펜던시가 없으면 재렌더링되고 업데이트가 될 때 위의 코드가 실행되지 않음, 즉 1번만 실행됨 
  // 따라서 컴포넌트 mount시 1회만 실행하고 싶을때 [] 에 처리함

useEffect(()=>{
    if(isNaN(num) == true){
      console.log('그러지마세요')
    }else{

    }


}, [num])//num이라는 변수가 변할 때만 useEffect 안의 코드가 실행된다
 

    return(
      <div className="container">

{
  timer == true ? <Timer></Timer> : null
}
{
  
}

       
         <input onChange={(e)=>{
          setNum(e.target.value);
          //console.log(inputValue)
          //console.log(isNaN(inputValue) )
          //isNaN(inputValue) == true ? inputValueChange(''): console.log('숫자')

         }} value={inputValue}></input>
         <button onClick={()=>{
          
         }}>초기화</button>
        <div>
          <p>값</p>
          {inputValue}
        </div>





        <div className="row">
          <button onClick={()=>{setCount(count+1)}}>버튼</button>
        </div>


        {count}


        <div className="row">
          <div className="col-md-6">
            <img src="https://codingapple1.github.io/shop/shoes1.jpg" width="100%" />
          </div>
          <div className="col-md-6">
            <h4 className="pt-5">{findItem.title}</h4>
            <p>{findItem.content}</p>
            <p>{findItem.price}원</p>
            <button className="btn btn-danger">주문하기</button> 
          </div>
        </div>
      </div> 

    )
  }



  function Timer(){
    return(
      <div className="alert alert-warning">
        2초이내 구매시 할인
      </div>
    )
    
  }
  export default Detail;