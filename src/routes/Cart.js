import {useMemo, memo, useState, useTransition, useDeferredValue} from "react";
import React from 'react';
import {Table} from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import {addCount} from './../store.js';
import { increase, changeName} from './../store/userSlice.js';



/* let Child = memo(function(){ //꼭 필요할 때만 재렌더링하려면 memo
    console.log('재렌더링됨')
    return (<div>자식임</div>)
})
 */


function 함수(){
    return '반복문 10억번 돌린 결과'
}
//let test = new Array(10000).fill(0)
let test = new Array(100).fill(0)

function Cart() {

    let state = useSelector((state)=>{ return state })//return state.user 이경우 user state 만 가져옴
    // ((state)=>state.user )  이렇게 생략가능
    //console.log(state.user)
    //console.log(state)
    //console.log(state.stock)
    //console.log(state.cart)
    let dispatch = useDispatch() // store.js로 요청보내주는 함수
    let [count, setCount] = useState(0)

    let result = useMemo(()=>{ return 함수()}, [state]);
    //렌더링 될때 실행됨 , useEffect와 실행시점만 차이가 있음
    let [name, setName] = useState('')
    let [isPending, startTransition] = useTransition()//startTransition = 늦게처리
//isPending 은 startTransition이 처리중일 때 true로 변함 

let state2 = useDeferredValue(name)

    return (
        <div>
            <input onChange={(e)=>{
                startTransition(()=>{
                    setName(e.target.value)
                })
               }
            } />
            {
                isPending ? '로딩중' : 
                test.map((a,i)=>{
                    return (<div key={i}>{/* {name} */}{state2}</div>)
                })
            }

            {/* <Child count={count}></Child>
            <button onClick={()=>{setCount(count+1)}}>+</button> */}

            <h6>{state.user.name} {state.user.age}의 장바구니</h6>
            <button onClick={()=>{
                
                dispatch(changeName())//changeName()실행해달라고 store.js에 부탁
            }}>버튼</button>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>상품명</th>
                            <th>수량</th>
                            <th>변경하기</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            state.cart.map((a, i)=>{
                                return (
                                <tr key={i}>
                                    <td>{state.cart[i].id}</td>
                                    <td>{state.cart[i].name}</td>
                                    <td>{state.cart[i].count}</td>
                                    <td>
                                        <button onClick={()=>{
                                            dispatch(addCount(state.cart[i].id))//지금 액션이 들어온 id를 찾아서 값을 올려줘야하기에 payload로 아이디를 적어줘야 함
                                        // dispatch(addCount(i))
                                            //cf)dispatch : 메세지보내다 뜻
                                        }}>+</button>

                                    </td>
                                </tr>
                                )
                            })
                        }
                    
                    
                    </tbody>
                </Table> 
        </div>
    )
}
export default Cart
