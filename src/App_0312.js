
import {useEffect, useState} from "react";
import {Button,Navbar,Container,Nav} from 'react-bootstrap';
import './App.css';
import shoes1 from './img/shoes1.jpg';
import shoes2 from './img/shoes2.jpg';
import shoes3 from './img/shoes3.jpg';
/* import 작명 from './data.js'; */
import data from './data.js';
import {Routes, Route, useNavigate, Outlet} from 'react-router-dom';
import Detail from "./routes/Detail.js";
import axios from "axios";//브라우저, Node.js를 위한 Promise API를 활용하는 HTTP 비동기 통신 라이브러리

function App() {

  let [shoes, setShoes] = useState(data);
  let navigate = useNavigate();//페이지 이동을 도와주는 함수

  let [count, setCount] = useState(0)

  return (
    <div className="App">


      <Navbar bg="dark" variant="dark" className='navMenu'>
        <Container>
          <Navbar.Brand onClick={()=>{navigate('/')}}>exShop</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link onClick={()=>{navigate('/')}}>Home</Nav.Link>
            <Nav.Link onClick={()=>{navigate('/detail/0')}}>Detail</Nav.Link>
            <Nav.Link onClick={()=>{navigate('/about')}}>about</Nav.Link>
            <Nav.Link onClick={()=>{navigate('/event/one')}}>event</Nav.Link>
          </Nav>
        </Container>
      </Navbar>

      <Routes>
        <Route path="/" element={
          <>
            <div className='main-bg'></div>
            <div className='container'>
              <div className='row'>
                {
                  shoes.map(function(a,i){//shoes 갯수 만큼 반복
                    return(
                      <Card shoes = {shoes[i]} i={i} key={i} navigate = {navigate}></Card>
                    )
                  })
                }
                
              </div>
              <button className="btn btn-primary more" onClick={()=>{
                setCount(count+1)
                console.log(count)
                if(count==0){
                  //console.log('로딩중 ui 띄우기') 
                   // 버튼 클릭시 로딩중 UI를 띄운다.
                   document.querySelector(".more").textContent = '로딩중';
                  axios.get('https://codingapple1.github.io/shop/data2.json')
                  .then((결과)=>{
                  // console.log(결과.data);
                    //console.log(shoes)
                    document.querySelector(".more").textContent = '더보기';
                    let copy = [ ...shoes, ...결과.data];
                    setShoes(copy);
                    //console.log('로딩중 ui 숨기기') 

                  })
                }

                else if(count==1){
                  axios.get('https://codingapple1.github.io/shop/data3.json')
                  .then((결과)=>{
                    // console.log(결과.data);
                     //console.log(shoes)
                     let copy2 = [ ...shoes, ...결과.data];
                     setShoes(copy2);
                   })
                }else{
                    console.log('상품이 없습니다.');
                    count = 0;
                }


                // Promise.all([axios.get('/url1'), axios.get('/url2')])//동시에 ajax 요청 여러개 할때
                // .then(()=>{ //위 2개의 요청이 성공했을 경우
 
                // })
                //axios.post('/',{name:'kim'}) //서버로 데이터전송하는 POST 요청



              }}>더보기</button> 
            {/* 응용 : 버튼 2회 누를 때 7,8,9 번 상품 가져오려면? 버튼누른 횟수저장해두면 좋을듯
                응용: 버튼 3회 누를 때는 상품더 없다고 알려주기
                응용 : 버튼 누르면 로딩중입니다 글자 띄우기
            */}

            </div> 
          </>
        }/>
        <Route path="/detail/:id" element={<Detail shoes={shoes}></Detail>} />

        <Route path="/about" element={<About/>}>
          <Route path="member" element={<div>멤버</div>} />
          <Route path="location" element={<div>위치정보</div>} />
        </Route>
        
        <Route path="/event" element={<Event/>}>
          <Route path="one" element={<div>첫 주문시 양배추즙 서비스</div>} />
          <Route path="two" element={<div>생일기념 쿠폰받기</div>} />
        </Route>

        <Route path="*" element={<div>없는 페이지입니다.</div>} />
      </Routes>

    </div>
  );
}


function Event(){
  return(
    <div>
      <h4 className="title">오늘의 이벤트</h4>
      <Outlet></Outlet>
    </div>
  )
}


function About(){
  return(
    <div>
      <h4 className="title">회사정보</h4>
      <Outlet></Outlet>
    </div>
  )
}



function Card(props){
//console.log(props.i)
  return(
    <div className='col-md-4'>
      <img src={'https://codingapple1.github.io/shop/shoes'+ (props.i+1) + '.jpg'} width="80%" onClick={()=>{props.navigate('/detail/'+(props.i))}} />
        <h4>{props.shoes.title} </h4>
        <p>{props.shoes.price}</p>
    </div>
  )
}


export default App;
